#pragma once

#include <iostream>
#include <string>
#include <sstream>

#include <WS2tcpip.h>

#pragma comment (lib, "ws2_32.lib")

#define MAX_BUFFER_SIZE (4096)

class CTcpMultiClientChat
{

public:

	CTcpMultiClientChat(const char* ipAddress, int port) :
		s_ipAddress(ipAddress), s_port(port) { }

	~CTcpMultiClientChat()
	{
		WSACleanup(); // Cleanup WinSock
	}

	int Init(); // Initialize the listening socket

	int Run(); // Run the server

protected:
	
	void GetSocketInfo(sockaddr_in socket_info, char* host_name, char* host_ip, char* service);

	void sendToClients(SOCKET curentSocket, const char* buf); // Send a message from a client to all others

private:

	const char* s_ipAddress;	// IP Address server will run on
	int	s_port;					// Port number for the web service
	SOCKET listeningSocket;		// Internal FD for the listening socket
	fd_set master;				// Master file descriptor set

	char host_name[NI_MAXHOST]; // Client's remote name
	char host_ip[NI_MAXSERV];	// Client's ip address
	char service[NI_MAXSERV];	// Service (i.e. port) the client is connect on
};