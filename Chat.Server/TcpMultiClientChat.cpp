#include "TcpMultiClientChat.h"


int CTcpMultiClientChat::Init()
{
	// Initialize WinSock
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		std::cerr << "Error at WSAStartup: " << WSAGetLastError() << std::endl;
		return SOCKET_ERROR;
	}

	// Create a socket
	listeningSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (listeningSocket == INVALID_SOCKET) {
		std::cerr << "Error at socket creation: " << WSAGetLastError() << std::endl;
		WSACleanup();
		return SOCKET_ERROR;
	}

	// Bind the ip address and port to a socket, then tell WinSock the socket is for listening 
	sockaddr_in serverSocket_info;
	serverSocket_info.sin_family = AF_INET;
	serverSocket_info.sin_port = htons(s_port);
	inet_pton(AF_INET, s_ipAddress, &serverSocket_info.sin_addr);

	bind(listeningSocket, (sockaddr*)&serverSocket_info, sizeof(serverSocket_info));

	listen(listeningSocket, SOMAXCONN);


	FD_ZERO(&master); // Create the master file descriptor set and zero it

	FD_SET(listeningSocket, &master); // Add listening socket as the first in set

	return 0;
}

int CTcpMultiClientChat::Run()
{
	while (true) {

		fd_set copy = master; // Make a copy of the master file descriptor set

		int socketCount = select(0, &copy, nullptr, nullptr, nullptr); // See what socket is active

		// Loop through all the current connections / potential connect
		for (int i = 0; i < socketCount; i++) {

			SOCKET curentSocket = copy.fd_array[i];

			if (curentSocket == listeningSocket) { // It is a connection request

				sockaddr_in clientSocket_info;
				int clientSize = sizeof(clientSocket_info);

				SOCKET clientSocket = accept(listeningSocket, (sockaddr*)&clientSocket_info, &clientSize); // Accept a new connection

				FD_SET(clientSocket, &master); // Add the new connection to the list of connected clients

				// Get name info about the client connected
				GetSocketInfo(clientSocket_info, host_name, host_ip, service);
				std::cout << host_name << " (" << host_ip << ") connected on port " << service << std::endl;

				// Send a welcome message to the connected client
				std::string welcomeMsg = "Connected to Server!";
				send(clientSocket, welcomeMsg.c_str(), welcomeMsg.size() + 1, 0);
			}
			else { // It is a message from client

				char buf[MAX_BUFFER_SIZE];
				ZeroMemory(buf, MAX_BUFFER_SIZE);

				// Receive message
				int bytesIn = recv(curentSocket, buf, MAX_BUFFER_SIZE, 0);
				if (bytesIn == SOCKET_ERROR) {
					closesocket(curentSocket);
					FD_CLR(curentSocket, &master); // Remove recent active client from the fd_set
				}
				else { //continue upway

					// Send message to other clients
					sendToClients(curentSocket, buf);
				}
			}
		}
	}

	FD_CLR(listeningSocket, &master); // Remove the listening socket from the fd_set
	closesocket(listeningSocket); // Close the socket

	// Message to let users know what's happening.
	std::string msg = "Server is shutting down. Goodbye";

	while (master.fd_count > 0) {

		// Get the socket number
		SOCKET sock = master.fd_array[0];

		// Send the goodbye message
		send(sock, msg.c_str(), msg.size() + 1, 0);

		// Remove it from the master file list and close the socket
		FD_CLR(sock, &master);
		closesocket(sock);
	}
	
	return 0;
}

void CTcpMultiClientChat::GetSocketInfo(sockaddr_in socket_info, char* host_name, char* host_ip, char* service)
{
	// Get name info about the client connected
	ZeroMemory(host_name, NI_MAXHOST); // same as memset(host, 0, NI_MAXHOST);
	ZeroMemory(host_ip, NI_MAXSERV);
	ZeroMemory(service, NI_MAXSERV);

	inet_ntop(AF_INET, &socket_info.sin_addr, host_ip, NI_MAXSERV);

	getnameinfo((sockaddr*)&socket_info, sizeof(socket_info), host_name, NI_MAXHOST, service, NI_MAXSERV, 0);
}

void CTcpMultiClientChat::sendToClients(SOCKET curentSocket, const char* buf)
{
	// Send message to other clients
	for (int i = 0; i < master.fd_count; i++) {

		SOCKET outSock = master.fd_array[i];
		if (outSock != listeningSocket && outSock != curentSocket) {

			std::ostringstream ss;
			ss << "SOCKET #" << curentSocket << ": " << buf;
			std::string strOut = ss.str();

			send(outSock, strOut.c_str(), strOut.size() + 1, 0);
		}
	}
}