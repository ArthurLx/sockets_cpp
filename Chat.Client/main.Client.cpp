#include <iostream>
#include <string>
#include <WS2tcpip.h>

#pragma comment(lib, "ws2_32.lib")

SOCKET connectionSocket;

void ClientHandler()
{
	char buf[4096]; // for receiving
	while (true) {

		ZeroMemory(buf, 4096);
		int bytesReceived = recv(connectionSocket, buf, 4096, 0);
		if (bytesReceived == SOCKET_ERROR) {
			std::cout << "SERVER LOST" << std::endl;
			break;
		}

		std::cout << std::string(buf, 0, bytesReceived) << std::endl;
		std::cout << "> ";
	}
	return;
}

int main()
{
	// Initialize WinSock
	WSAData wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		std::cerr << "Error at WSAStartup: " << WSAGetLastError() << std::endl;
		return 1;
	}

	// Create a socket
	connectionSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (connectionSocket == INVALID_SOCKET) {
		std::cerr << "Error at socket creation: " << WSAGetLastError() << std::endl;
		WSACleanup();
		return 1;
	}

	// Create sockaddr_in structure with the info of ip address, port and address family OF THE SERVER
	std::string ipAddress = "127.0.0.1"; // IP Address of the server
	int port = 54000; // Listening port number on the server

	sockaddr_in serverSocket_info;
	serverSocket_info.sin_family = AF_INET;
	serverSocket_info.sin_port = htons(port);
	inet_pton(AF_INET, ipAddress.c_str(), &serverSocket_info.sin_addr);

	// Connect to server
	int connResult = connect(connectionSocket, (sockaddr*)&serverSocket_info, sizeof(serverSocket_info));
	if (connResult == SOCKET_ERROR) {
		std::cerr << "Error at connection to the server: " << WSAGetLastError() << std::endl;
		closesocket(connectionSocket);
		WSACleanup();
		system("pause");
		return 1;
	}

	CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandler, NULL, NULL, NULL);

	// While loop to send and receive data

	std::string userInput; // for sending
	while (true) {
		std::cout << "> ";
		std::getline(std::cin, userInput);

		// Make sure the user has typed in something
		if (userInput.size() > 0) {
			// Send the text
			//userInput = userInput + "/0";
			int sendResult = send(connectionSocket, userInput.c_str(), userInput.size() + 1, 0);
			if (sendResult != SOCKET_ERROR) {

			}
		}

		Sleep(10);
	}

	closesocket(connectionSocket); // Close the socket
	WSACleanup(); // Cleanup WinSock

	return 0;
}